CC=gcc
PREFIX = /usr
BINDIR = $(PREFIX)/bin

all: touchpad_config

touchpad_config: touchpad_config.o
	$(CC) touchpad_config.o -o touchpad_config

touchpad_config.o: touchpad_config.c
	$(CC) -c touchpad_config.c

install: all
	install -Dm755 touchpad_config "$(DESTDIR)$(BINDIR)/touchpad_config"

clean:
	rm -f *.o touchpad_config

